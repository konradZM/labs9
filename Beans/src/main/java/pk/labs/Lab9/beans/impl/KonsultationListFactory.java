/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

/**
 *
 * @author st
 */
import pk.labs.Lab9.beans.*;
import java.beans.*;
import java.io.*;

public class KonsultationListFactory implements ConsultationListFactory, Serializable {
    public KonsultationListFactory() {}
    public ConsultationList create()
    {
        ConsultationList nowy = new ListaKonsultacji();
        return nowy;
    }
    public ConsultationList create(boolean deserialize)
    {
        
        if (deserialize) {
            ConsultationList nowy = new ListaKonsultacji();
            try{
                XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("ziarno.xml")));
                Object object = decoder.readObject();
                decoder.close();
                
                nowy = (ListaKonsultacji)object;
                
            } catch(FileNotFoundException e)
            { e.getStackTrace();}
            return nowy;
        }
        else
        {
            return new ListaKonsultacji();
        }
    }
    public void save(ConsultationList consultationList)
    {
        try{
                XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("ziarno.xml")));
                encoder.writeObject(consultationList);
                encoder.close();
                
            } catch(FileNotFoundException e)
            { e.getStackTrace();}
    }
}