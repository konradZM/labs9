/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
//package pk.labs.Lab9.beans;

/**
 *
 * @author Konrad Czarnota
 */
import pk.labs.Lab9.beans.*;
import java.beans.*;
import java.util.Date;
import java.io.*;


public class Konsultacja implements Consultation, Serializable {
    private String student;
    private Term termin;
    public Konsultacja() {}
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        pcs.addPropertyChangeListener(listener);
    }
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        pcs.removePropertyChangeListener(listener);
    }
    public String getStudent()
    {
        return this.student;
    }
    public void setStudent(String student)
    {
        this.student = student;
    }
    public Date getBeginDate()
    {
        return this.termin.getBegin();
    }
    public Date getEndDate()
    {
        return this.termin.getEnd();
    }
    public void setTerm(Term term) throws PropertyVetoException
    {
        Term oldTerm = termin;
        termin = term;
        pcs.firePropertyChange("value", oldTerm, term);
    }
    public void prolong(int minutes) throws PropertyVetoException
    {
        if (minutes<45) {
            Term oldTerm = termin;
            termin.setDuration(minutes);
            pcs.firePropertyChange("value", oldTerm, termin);
        }
    }
}
