/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
//package pk.labs.Lab9.beans;
/**
 *
 * @author Konrad Czarnota
 */
import pk.labs.Lab9.beans.Term;
import java.util.Date;
import java.io.*;

public class Termin implements Term, Serializable {
    private Date poczatek;
    private int czasTrwania; 
    public Termin() {}
    public Date getBegin()
    {
        return this.poczatek;
    }
    public void setBegin(Date begin)
    {
        
        this.poczatek = begin;
    }
    public Date getEnd()
    {
        long t = poczatek.getTime();
        Date koniec = new Date(t + czasTrwania*60000);
        return koniec;
    }
    public int getDuration()
    {
       return this.czasTrwania;
    }
    public void setDuration(int duration)
    {
        if (duration>0) {
            this.czasTrwania = duration;
        }
    }
}
